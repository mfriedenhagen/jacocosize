package de.friedenhagen.jacocosize;

public class App {
    private final String name;
    private final int age;

    public App(String name, int age){
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "App{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
