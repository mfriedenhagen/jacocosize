package de.friedenhagen.jacocosize;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AppTest {
    @Test
    public void testToString() throws Exception {
        final App sut = new App("me", 18);
        assertEquals("App{name='me', age=18}", sut.toString());
    }
}